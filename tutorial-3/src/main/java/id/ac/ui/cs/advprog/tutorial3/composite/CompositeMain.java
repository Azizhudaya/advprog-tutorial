package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;

import java.util.List;

public class CompositeMain {
    public static void main(String[] args) {
        Company medkom = new Company();

        //init all employee here
        Ceo ceo = new Ceo("Aziz", 250000);
        Cto cto = new Cto("Fikri", 200000);
        FrontendProgrammer fend = new FrontendProgrammer("Hudaya", 60000);
        NetworkExpert netEx = new NetworkExpert("Fahmi", 80000);

        //add employee to Medkom
        medkom.addEmployee(ceo);
        medkom.addEmployee(cto);
        medkom.addEmployee(fend);
        medkom.addEmployee(netEx);

        //print all properties here
        System.out.println("Welcome to Medcom, where all medicine connected!");
        System.out.println("he're all the employee! :"); 
        
        List<Employees> listEmployee = medkom.getAllEmployees();

        for (Employees employees : listEmployee) {
            System.out.println(employees.getName() + " as " + employees.getRole()
                + " with salary " + employees.getSalary());
        }

        System.out.println("with all our salaries is: " + medkom.getNetSalaries());




    }
}