package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMain {
    public static void main(String[] args) {
        Food thicBurger = BreadProducer.THICK_BUN.createBreadToBeFilled();

        //adding condiments to thicburger
        thicBurger = FillingDecorator.BEEF_MEAT.addFillingToBread(thicBurger);
        thicBurger = FillingDecorator.LETTUCE.addFillingToBread(thicBurger);
        thicBurger = FillingDecorator.TOMATO.addFillingToBread(thicBurger);
        thicBurger = FillingDecorator.TOMATO_SAUCE.addFillingToBread(thicBurger);

        Food thinBurger = BreadProducer.THIN_BUN.createBreadToBeFilled();

        thinBurger = FillingDecorator.BEEF_MEAT.addFillingToBread(thinBurger);
        thinBurger = FillingDecorator.CHEESE.addFillingToBread(thinBurger);
        thinBurger = FillingDecorator.CUCUMBER.addFillingToBread(thinBurger);
        thinBurger = FillingDecorator.BEEF_MEAT.addFillingToBread(thinBurger);

        //check thick burer
        System.out.println("i ordered a: " + thicBurger.getDescription() + "with cost : " 
            + thicBurger.cost());

        //check thin burer
        System.out.println("i ordered a: " + thinBurger.getDescription() + "with cost : " 
            + thinBurger.cost());

        double costAll = thicBurger.cost() + thinBurger.cost();

        System.out.println("and now i want to pay " + costAll + " Dollar");
    }
}