package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    public ModelDuck() {
        this.setQuackBehavior(new MuteQuack());
        this.setFlyBehavior(new FlyNoWay());  
    }

    public void display() {
        System.out.println("i'm a real Model Duck");
    }
}
